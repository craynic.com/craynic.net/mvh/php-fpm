FROM composer:2@sha256:d702aa6a31321b7c2f7e4258334ec965c2813859a2db3617b8a9f746b44e42c2 AS composer

FROM php:7.4.33-fpm-alpine@sha256:0aeb129a60daff2874c5c70fcd9d88cdf3015b4fb4cc7c3f1a32a21e84631036

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions \
    /usr/local/bin/

RUN apk add --no-cache \
        bash~=5 \
        supervisor~=4 \
        msmtp~=1 \
        inotify-tools~=3 \
        run-parts~=4 \
        logrotate~=3 \
        iproute2~=5 \
    && apk upgrade --no-cache \
        # various security issues fixed in 7.83.1-r5
        curl libcurl \
        # various security issues fixed in 1.1.1t-r0
        openssl \
    && apk add --no-cache --virtual .build-deps \
        git~=2 \
    && chmod +x /usr/local/bin/install-php-extensions \
    && install-php-extensions \
        amqp \
        bcmath \
        bz2 \
        calendar \
        enchant \
        exif \
        gd \
        gettext \
        gmp \
        imagick \
        imap \
        intl \
        mailparse \
        memcached \
        mongodb \
        mysqli \
        oauth \
        opcache \
        pdo_mysql \
        pspell \
        redis \
        soap \
        tidy \
        xsl \
        yaml \
        zip \
    && apk del .build-deps

ENV MAIL_RELAYHOST="" \
    MAIL_TLS_KEYFILE="" \
    MAIL_TLS_CRTFILE="" \
    MAIL_TLS_CAFILE="" \
    PHP_POOL_D="" \
    PHP_CONTAINER_DEBUGGING=false

COPY files/ /
COPY --from=composer /usr/bin/composer /usr/local/bin/composer

WORKDIR /var/www

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/healthcheck.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]
