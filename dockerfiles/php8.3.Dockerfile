FROM composer:2@sha256:d702aa6a31321b7c2f7e4258334ec965c2813859a2db3617b8a9f746b44e42c2 AS composer

FROM php:8.3.17-fpm-alpine@sha256:3ebbe25803556a210e1d4da0f9c8bd604cc56f9872c2c6408095a85672a58e41

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions \
    /usr/local/bin/

RUN apk add --no-cache \
        bash~=5 \
        supervisor~=4 \
        msmtp~=1 \
        inotify-tools~=4 \
        run-parts~=4 \
        logrotate~=3 \
        iproute2~=6 \
    && apk add --no-cache --virtual .build-deps \
        git~=2 \
    && chmod +x /usr/local/bin/install-php-extensions \
    && install-php-extensions \
        amqp \
        bcmath \
        bz2 \
        calendar \
        enchant \
        exif \
        gd \
        gettext \
        gmp \
        imagick \
        imap \
        intl \
        mailparse \
        memcached \
        mongodb \
        mysqli \
        oauth \
        opcache \
        pdo_mysql \
        pspell \
        redis \
        soap \
        tidy \
        xsl \
        yaml \
        zip \
    && apk del .build-deps

ENV MAIL_RELAYHOST="" \
    MAIL_TLS_KEYFILE="" \
    MAIL_TLS_CRTFILE="" \
    MAIL_TLS_CAFILE="" \
    PHP_POOL_D="" \
    PHP_CONTAINER_DEBUGGING=false

COPY files/ /
COPY --from=composer /usr/bin/composer /usr/local/bin/composer

WORKDIR /var/www

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/healthcheck.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]
