#!/usr/bin/env bash

set -Eeuo pipefail

read -ra PORTS <<< "$HEALTHCHECK_PORTS"

# check open ports
if (( ${#PORTS[@]} != 0 )); then
  # bash would exit with code 127 if the command is not present; let's turn it into exit code 2
	[[ -x $(which ss) ]] || { echo "ss command not found" >&2; exit 2; }

	SS_OUT=$(ss -ltnH)

	for port in "${PORTS[@]}"; do
		grep -q ":$port " <<< "$SS_OUT" || { echo "Listening port check failed for port $port" >&2; exit 1; }
	done
fi

# explicit exit code
exit 0
