#!/usr/bin/env bash

set -Eeuo pipefail

PHP_POOL_DIR=${PHP_POOL_D:-"/etc/php-fpm.d/"}

inotifywait-dir.sh "$PHP_POOL_DIR" | while read -r line; do
  echo "$line"

  /usr/local/sbin/reload-php.sh
done
