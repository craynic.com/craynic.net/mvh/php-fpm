#!/usr/bin/env bash

set -Eeuo pipefail

PHP_FPM_CONF_FILE="/usr/local/etc/php-fpm.conf"

if [[ "$PHP_POOL_D" != "" ]]; then
  sed -i "s/^\(include\s*=.*$\)//" -- "$PHP_FPM_CONF_FILE"
  echo "include = ${PHP_POOL_D%/}/*.conf" >> "$PHP_FPM_CONF_FILE"
fi

trap 'kill -USR1 "${CHILD_PID}"; wait "${CHILD_PID}"' SIGUSR1
trap 'kill -USR2 "${CHILD_PID}"; wait "${CHILD_PID}"' SIGUSR2
trap 'kill -QUIT "${CHILD_PID}"; wait "${CHILD_PID}"; exit 0' SIGQUIT
trap 'kill -INT "${CHILD_PID}"; wait "${CHILD_PID}"; exit 0' SIGINT
trap 'kill -TERM "${CHILD_PID}"; wait "${CHILD_PID}"; exit 0' SIGTERM

/usr/local/sbin/php-fpm -c /usr/local/etc/php/php-fpm.ini -y /usr/local/etc/php-fpm.conf -O &
CHILD_PID="$!"
wait "${CHILD_PID}"
