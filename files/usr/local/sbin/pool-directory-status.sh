#!/usr/bin/env bash

set -Eeuo pipefail

PHP_POOL_DIR=${PHP_POOL_D:-"/etc/php-fpm.d/"}

if [[ ! -f "${PHP_POOL_DIR}/.ready" ]]; then
  echo "Pool directory $PHP_POOL_DIR is not ready" >&2
  exit 1
elif ! compgen -G "$PHP_POOL_DIR/*.conf" >/dev/null; then
  echo "Pool directory $PHP_POOL_DIR is empty" >&2
  exit 2
fi

# explicit exit code
exit 0
