#!/usr/bin/env bash

set -Eeuo pipefail

if ! /usr/local/sbin/pool-directory-status.sh; then
  if supervisorctl status php-fpm >/dev/null; then
    echo "Gracefully quitting php-fpm."
    supervisorctl stop php-fpm
  fi
else
  # reload PHP-FPM
  if supervisorctl status php-fpm >/dev/null; then
    supervisorctl signal USR2 php-fpm
  else
    supervisorctl start php-fpm
  fi
fi
